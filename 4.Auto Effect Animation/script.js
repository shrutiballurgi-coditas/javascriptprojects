const containerElement=document.querySelector(".container");
const companies=["Coditas","Amazon","Google"];
let companiesIndex=0;
let characterIndex=0;

updateText();
function updateText(){
    characterIndex++;

    containerElement.innerHTML=`<h1>I am an employee at ${companies[companiesIndex].slice(0,characterIndex)}.</h1>`;
    if(characterIndex===companies[companiesIndex].length){
        companiesIndex++;
        characterIndex=0;
    }
    if(companiesIndex === companies.length){
        companiesIndex=0;
    }
    setTimeout(updateText,600);
}