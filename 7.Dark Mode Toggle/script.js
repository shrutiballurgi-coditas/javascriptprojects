const checkBoxElement=document.querySelector(".check-box");
const bodyElement=document.querySelector("body");
checkBoxElement.checked=JSON.parse(localStorage.getItem("mode"));
updateBody();
function updateBody(){
  if(checkBoxElement.checked){
    bodyElement.style.background="black";
  } 
  else{
    bodyElement.style.background="white";
  }
}
checkBoxElement.addEventListener("checkBoxElement",()=>{
  updateBody();
  updateLocalStorage();
});
function updateLocalStorage(){
  localStorage.setItem("mode",JSON.stringify(checkBoxElement.checked));
}
