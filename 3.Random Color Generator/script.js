const containerElement=document.querySelector(".main-container");

for (let index=0; index<30; index++) {
  const colorContainerElement=document.createElement("section");
  colorContainerElement.classList.add("color-container");
  containerElement.appendChild(colorContainerElement);
}

const colorContainerElement=document.querySelectorAll(".color-container");

generateColors();

function generateColors() {
  colorContainerElement.forEach((colorContainerEl) => {
    const newColorCode=randomColor();
    colorContainerEl.style.backgroundColor="#"+newColorCode;
    colorContainerEl.innerText="#"+newColorCode;
  });
}

function randomColor() {
  const chars="0123456789abcdef";
  const colorCodeLength=6;
  let colorCode="";
  for (let index=0; index<colorCodeLength; index++) {
    const randomNum = Math.floor(Math.random() * chars.length);
    colorCode += chars.substring(randomNum, randomNum+1);
  }
  return colorCode;
}